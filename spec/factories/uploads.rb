# frozen_string_literal: true

FactoryBot.define do
  factory :upload do
    # upload_with_images will create image data after the upload has been created
    factory :upload_with_images do
      # images_count is decleared as a transient attribute available in the
      # callback via the evaluator
      transient do
        images_count { 5 }
      end

      images do
        Array.new(images_count) { association(:image) }
      end
    end
    user
  end
end
