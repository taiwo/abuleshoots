# frozen_string_literal: true

FactoryBot.define do
  factory :image do
    title { Faker::Coffee.blend_name }
    description { Faker::Lorem.sentence(word_count: 16) }
    file { Rack::Test::UploadedFile.new("spec/files/#{rand(1..20)}-min.jpg") }
    is_private { Faker::Boolean.boolean(true_ratio: 0.2) }
    upload
  end
end
