# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Image, type: :model do
  before(:all) do
    @image = create(:image)
  end

  describe 'associations' do
    it { should belong_to(:upload) }
  end

  describe 'attachements' do
    it 'has attached file' do
      expect(@image.file).to be_attached
    end
  end

  describe 'validations' do
    it { should validate_presence_of(:title) }
    it { should validate_presence_of(:description) }
    it { should validate_length_of(:title).is_at_most(30) }
    it { should validate_length_of(:description).is_at_most(160) }
  end
end
