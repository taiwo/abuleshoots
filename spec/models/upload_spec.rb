# frozen_string_literal: true

require 'rails_helper'

RSpec.describe Upload, type: :model do
  before(:all) do
    @upload = create(:upload)
  end

  it 'should have valid attributes' do
    expect(@upload).to be_valid
  end

  describe 'associations' do
    it { should have_many(:images) }
    it { should accept_nested_attributes_for(:images) }
    it { should belong_to(:user) }
  end
end
