# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/uploads', type: :request do
  let(:user) { create(:user) }
  let(:upload) { build(:upload_with_images, images_count: 1) }

  let(:valid_attributes) do
    {
      images_attributes: {
        id: upload.images.first.id,
        title: upload.images.first.title,
        description: upload.images.first.description,
        is_private: upload.images.first.is_private
      }
    }
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      sign_in user
      upload = Upload.new valid_attributes
      upload.user = user
      upload.save
      get uploads_url
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      sign_in user
      upload = Upload.new valid_attributes
      upload.user = user
      upload.save
      get upload_url(upload)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      sign_in user
      get new_upload_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      sign_in user
      upload = Upload.new valid_attributes
      upload.user = user
      upload.save
      get edit_upload_url(upload)
      expect(response).to be_successful
    end
  end

  describe 'POST /create' do
    context 'with valid parameters' do
      it 'creates a new Upload' do
        expect do
          sign_in user
          post uploads_url, params: { upload: valid_attributes }
        end.to change(Upload, :count).by(1)
      end

      it 'redirects to the created upload' do
        sign_in user
        post uploads_url, params: { upload: valid_attributes }
        expect(response).to redirect_to(dashboard_url)
      end
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_attributes) do
        skip('Add a hash of attributes valid for your model')
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested upload' do
      sign_in user
      upload = Upload.new valid_attributes
      upload.user = user
      upload.save
      expect do
        upload.images.each do |image|
          delete image_url(image)
        end
        delete upload_url(upload)
      end.to change(Upload, :count).by(-1)
    end

    it 'redirects to the uploads list' do
      sign_in user
      upload = Upload.new valid_attributes
      upload.user = user
      upload.save
      upload.images.each do |image|
        delete image_url(image)
      end
      delete upload_url(upload)
      expect(response).to redirect_to(uploads_url)
    end
  end
end
