# frozen_string_literal: true

require 'rails_helper'

RSpec.describe '/images', type: :request do
  let(:user) { create(:user) }
  let(:upload) { create(:upload, user: user) }
  let(:image) { build(:image) }
  let(:valid_attributes) do
    {
      title: image.title,
      description: image.description,
      is_private: image.is_private,
      upload_id: upload.id
    }
  end

  let(:invalid_attributes) do
    {
      title: nil,
      description: nil,
      upload_id: nil
    }
  end

  describe 'GET /index' do
    it 'renders a successful response' do
      Image.create! valid_attributes
      get images_url
      expect(response).to be_successful
    end
  end

  describe 'GET /show' do
    it 'renders a successful response' do
      image = Image.create! valid_attributes
      get image_url(image)
      expect(response).to be_successful
    end
  end

  describe 'GET /new' do
    it 'renders a successful response' do
      sign_in user
      get new_image_url
      expect(response).to be_successful
    end
  end

  describe 'GET /edit' do
    it 'render a successful response' do
      sign_in user
      image = Image.create! valid_attributes
      get edit_image_url(image)
      expect(response).to be_successful
    end
  end

  describe 'PATCH /update' do
    context 'with valid parameters' do
      let(:new_image) { build(:image, upload_id: upload.id) }
      let(:new_attributes) do
        {
          title: new_image.title,
          description: new_image.description
        }
      end

      it 'updates the requested image' do
        sign_in user
        image = Image.create! valid_attributes
        patch image_url(image), params: { image: new_attributes }
        image.reload
        expect(response).to have_http_status(302)
      end

      it 'redirects to the image' do
        sign_in user
        image = Image.create! valid_attributes
        patch image_url(image), params: { image: new_attributes }
        image.reload
        expect(response).to redirect_to(image_url(image))
      end
    end

    context 'with invalid parameters' do
      it "renders a successful response (i.e. to display the 'edit' template)" do
        sign_in user
        image = Image.create! valid_attributes
        patch image_url(image), params: { image: invalid_attributes }
        expect(response).to be_successful
      end
    end
  end

  describe 'DELETE /destroy' do
    it 'destroys the requested image' do
      sign_in user
      image = Image.create! valid_attributes
      expect do
        delete image_url(image)
      end.to change(Image, :count).by(-1)
    end

    it 'redirects to the images list' do
      sign_in user
      image = Image.create! valid_attributes
      delete image_url(image)
      expect(response).to redirect_to(images_url)
    end
  end
end
