# frozen_string_literal: true

Rails.application.routes.draw do
  devise_for :users
  resources :uploads
  resources :images
  root to: 'images#index'
  get 'dashboard', to: 'dashboard#index'
end
