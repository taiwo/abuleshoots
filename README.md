# AbuleShoots - Search & discover rustic images

This is basic image repository of Abule (meaning village/rustic/countryside) pictures. You can view a live version [here](https://abuleshoots.herokuapp.com).

## Table of contents

- [Technologies](#technologies)
- [Setup](#setup)
- [Features](#features)
- [Status](#status)
- [Test suite](#test-suite)
- [CI/CD](#ci/cd)

## Technologies

### Backend

- Ruby - 2.7.0p0
- Rails - 6.0.3.4
- Database - PostgreSQL
- Dropbox for image storage

### Frontend

- Bulma
- Stimulus Javascript

### TestSuite

- RSpec
- Faker
- FactortBot

## Setup

- Clone the project on your machine using: `git clone git@gitlab.com:taiwo/abuleshoots.git`
- Run `bundle install` to install needed gems
- Run `rails db:setup` to setup the database
- Launch the app using `rails server`
- Visit `http://localhost:3000/` in your browser
- You can login with email: `user@example.com` and password `password`

## Features

- Upload individual images
- Display images
- Upload multiple images
- Search images
- ~~Make index page tiles with mansory~~ Crop images
- User authentication
- Private and public image upload (permissions)
- Download Image
- Adjust index page interface
- Adjust authentication form interface
- Adjust nested forms
- Host images to cloud service (Dropbox)
- Deploy site
- Seed db

To-do list:
 
- Add tags to images
- Search with tags
- Suggest similar images

## Status

Project is: _in progress_

## Test suite

- You can run the test by running `bundle exec rspec`

## CI/CD
- To-do
