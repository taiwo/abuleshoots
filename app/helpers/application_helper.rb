# frozen_string_literal: true

module ApplicationHelper
  def link_to_add_fields(name, f, association)
    new_object = f.object.send(association).klass.new
    id = new_object.object_id

    fields = f.fields_for(association, new_object, child_index: id) do |builder|
      render("#{association.to_s.singularize}_fields", f: builder)
    end

    link_to(name, '#',
            class: 'add_fields column is-4 button is-outlined is-text is-inverted
            is-large has-text-weight-semibold has-text-success has-background-white',
            data: { id: id, fields: fields.gsub("\n", '') })
  end
end
