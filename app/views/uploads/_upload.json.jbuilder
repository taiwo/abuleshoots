# frozen_string_literal: true

json.extract! upload, :id, :created_at, :updated_at
json.url upload_url(upload, format: :json)
