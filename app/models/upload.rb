# frozen_string_literal: true

class Upload < ApplicationRecord
  belongs_to :user
  has_many :images, inverse_of: 'upload'

  accepts_nested_attributes_for :images, allow_destroy: true, reject_if: :all_blank
end
