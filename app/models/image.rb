# frozen_string_literal: true

class Image < ApplicationRecord
  default_scope { order('created_at DESC') }
  scope :is_private, -> { where(is_private: true) }
  scope :is_public, -> { where(is_private: false).or(where(is_private: nil)) }

  belongs_to :upload
  has_one_attached :file

  validates :title, :description, presence: true
  validates :title, length: { maximum: 30 }
  validates :description, length: { maximum: 160 }

  def download_image
    file.download
  end
end
