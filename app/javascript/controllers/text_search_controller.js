import {
    Controller
} from "stimulus"

export default class extends Controller {
    static classes = ["hidden"]
    static targets = ["filterInput", "image"]

    get filterValue() {
        return this.filterInputTarget.value
    }

    connect() {
        fetch(`/`, {
                headers: {
                    accept: 'application/json'
                }
            }).then((response) => response.text())
            .then(data => console.log(data))
    }
    filter() {
        this.imageTargets.forEach(image => {
            const visible = image.innerText.toLowerCase().search(this.filterValue) >= 0
            image.classList.toggle(this.hiddenClass, !visible)
        });
    }
}