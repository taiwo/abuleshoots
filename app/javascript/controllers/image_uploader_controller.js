import {
    Controller
} from "stimulus";

export default class extends Controller {
    static targets = ["textToChange"]

    changeFileName(event) {
        const eventTarget = event.currentTarget
        if (eventTarget.files.length > 0) {
            this.textToChangeTargets.forEach((textToChange) => {
                if (textToChange.id == event.target.id) {
                    textToChange.innerText = eventTarget.files[0].name
                }
            });
        }
    }
}