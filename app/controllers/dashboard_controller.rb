# frozen_string_literal: true

class DashboardController < ApplicationController
  before_action :authenticate_user!
  before_action :set_user

  def index
    @private_images = Image.is_private.includes(:upload).where(
      'uploads.user_id = ?', @user
    ).references(:uploads).reorder(
      'images.created_at DESC'
    )
    @public_images = Image.is_public.includes(:upload).where(
      'uploads.user_id = ?', @user
    ).references(:uploads).reorder(
      'images.created_at DESC'
    )
  end

  private

  def set_user
    @user = current_user
  end
end
