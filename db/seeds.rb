# frozen_string_literal: true

if User.count.zero?
  # Seed db with random users, uploads and images
  puts 'Uploading random images'
  FactoryBot.create_list(:upload_with_images, 5)
  puts 'Done'

  # Create example user
  puts 'Creating example user'
  FactoryBot.create(:user, email: 'user@example.com', password: 'password')
  puts 'Done'
  puts "You can login in with email: 'user@example.com' and password: 'password'"
end
