# frozen_string_literal: true

class AddUserRefToUploads < ActiveRecord::Migration[6.0]
  def change
    add_reference :uploads, :user, null: false, foreign_key: true
  end
end
