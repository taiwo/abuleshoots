# frozen_string_literal: true

class CreateImages < ActiveRecord::Migration[6.0]
  def change
    create_table :images do |t|
      t.string :title, limit: 30, comment: 'Title of the image with 30 characters limit'
      t.string :description, limit: 160, comment: 'Description of the image with 160 characters limit'

      t.timestamps
    end
  end
end
