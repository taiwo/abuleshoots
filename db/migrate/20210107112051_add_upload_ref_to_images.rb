# frozen_string_literal: true

class AddUploadRefToImages < ActiveRecord::Migration[6.0]
  def change
    add_reference :images, :upload, null: false, foreign_key: true, comment: 'Upload reference'
  end
end
