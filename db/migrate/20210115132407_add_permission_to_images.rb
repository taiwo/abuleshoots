# frozen_string_literal: true

class AddPermissionToImages < ActiveRecord::Migration[6.0]
  def change
    add_column :images, :is_private, :boolean, null: true, comment: 'Allow users to set image permission'
  end
end
